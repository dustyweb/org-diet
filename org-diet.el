;;; color-theme.el --- install color themes

;; Copyright (C) 2010-2014  Christopher Allan Webber <cwebber@dustycloud.org>

;; This file is not part of GNU Emacs.

;; This is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 2, or (at your option) any later
;; version.
;;
;; This is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
;; MA 02111-1307, USA.

(require 'org)
(require 'org-table)
(require 'ob-gnuplot)
(require 'cl)
(require 'yasnippet)


(defvar org-diet-gathering-funcs
  '(("weight" . org-diet-gather-weight)
    ("todo" . org-diet-gather-todo)
    ("calories" . org-diet-gather-calories))
  "Functions to run to gather data for analysis.

Functions should return a cons of ('Varname' . 'value') for every
piece of information gathered, or nil if nothing is to be
gathered.  Functions should expect to gather information from the
entry at point.  (maybe this will change later)")

(defcustom org-diet-file "~/org/diet.org"
  "Location of your diet file.")


(defun org-diet-gather-todo ()
  (org-get-todo-state))

(defun org-diet-gather-weight ()
  (let ((weight
         (org-entry-get (point) "Weight")))
    (if weight
        (string-to-number weight))))

(defun org-diet-gather-calories ()
  (let ((original-date (org-diet-entry-timestamp-fast)))
    (save-excursion
      (search-forward-regexp "^\\#\\+TBLFM")
      (previous-line)
      (if (equal original-date (org-diet-entry-timestamp-fast))
          (string-to-number (org-table-get-field 4))))))


(defun org-diet-entry-timestamp-fast ()
  "Get the entry date from this item if in the header.
Kind of like (org-entry-get (point) \"TIMESTAMP\") but faster and
only for timestamps on headers.

Also adds a text property with a marker of where this entry was."
  (let* ((entry-header (nth 4 (org-heading-components)))
         (date-match-start (string-match org-ts-regexp3
                                         entry-header)))
    (if date-match-start
        ; Return the date, but without the <>/[] chars
        (let ((date-string
               (substring entry-header
                          (+ date-match-start 1)
                          (- (match-end 0) 1))))
          ; Remember the position, and pre-parse the time for speed hacks
          (propertize date-string
                      'position (point-marker)
                      'absolute-time (org-time-string-to-absolute date-string))))))


(defun org-diet-gather-properties-at-point ()
  "Return an alist full of all properties at point.

Properties gathered from funcs specified in org-diet-gathering-funcs"
  (mapcar
   (lambda (gather-cons) 
     (let ((gather-property (car gather-cons))
           (gather-func (cdr gather-cons)))
       (cons gather-property
             (apply gather-func nil))))
   org-diet-gathering-funcs))


(defun org-diet-weight-subtree-entries-at-point ()
  "Get all the weight subtree entries at point, and add properties

Returns a list of (DATES OLDEST-DATE NEWEST-DATE)"
  (let ((oldest-date nil)
        (oldest-date-absolute nil)
        (newest-date nil)
        (newest-date-absolute nil))
    (list
     (cdr (org-map-entries
           (lambda ()
             (let* ((date (org-diet-entry-timestamp-fast))
                    (date-absolute (if date (get-text-property 0 'absolute-time date))))
               ;; Set the lowest date, if applicable
               (if (and date
                        (or
                         (not oldest-date)
                         (< date-absolute oldest-date-absolute)))
                   (setq oldest-date date
                         oldest-date-absolute date-absolute))
               (if (and date
                        (or
                         (not newest-date)
                         (> date-absolute newest-date-absolute)))
                   (setq newest-date date
                         newest-date-absolute date-absolute))
               (cons date
                     (org-diet-gather-properties-at-point))))
           nil 'tree))
     oldest-date newest-date)))


;; This is the slowish version
;;
;; (defun org-diet-weight-subtree-entries-at-point ()
;;   (cdr (org-map-entries
;;         '(cons (org-entry-get (point) "TIMESTAMP")
;;                (org-entry-get (point) "Weight"))
;;         nil 'tree)))

(defun org-diet-get-date-range-by-dates (oldest-date newest-date)
  "Return a range of dates starting with start-date and ending at date-length"
  (org-diet-get-date-range
   newest-date
   (- (org-time-string-to-absolute newest-date)
      (org-time-string-to-absolute oldest-date))))


(defun org-diet-time-string-from-difference (time-string difference)
  "Take TIME-STRING and difference (an integer) and return another time stringj

The new time string is the number of days since TIME-STRING.
TIME-STRING can be positive (forwards in time) or negative (backwards in time)"
  (format-time-string
   "%Y-%m-%d %a"
   (org-time-from-absolute
    (+ (org-time-string-to-absolute time-string) difference))))


(defun org-diet-time-string-today ()
  "Get today as a time string, org-diet/orgmode style"
  (format-time-string
   "%Y-%m-%d"
   (org-time-from-absolute
    (time-to-days (current-time)))))


(defun org-diet-time-string-difference (time-string-1 time-string-2)
  "See what the difference is between TIME-STRING-1 and TIME-STRING-2
This returns the difference between these two in days, an integer."
  (- (org-time-string-to-absolute time-string-1)
     (org-time-string-to-absolute time-string-2)))


(defun org-diet-get-date-range (start-date date-length)
  "Return a range of dates starting with start-date and ending at date-length"
  (let ((new-dates nil)
        (first-date-absolute (org-time-string-to-absolute start-date)))
    (dotimes (i date-length)
      (push
       (format-time-string
        "%Y-%m-%d %a"
        (org-time-from-absolute
         (- (org-time-string-to-absolute start-date) i)))
       new-dates))
    (reverse new-dates)))


;;; Not used at the sec... but maybe we'll canibalize soon

;; (defun org-diet-average-weight-prettify (weight-results)
;;   (list (format "%.2f" (car weight-results))
;;         (format "%d%%" (* (nth 1 weight-results) 100))))


(defun org-diet-gather-date-list (&optional diet-file)
  "Gather all properties in a diet file

Returns a list of (DATES OLDEST-DATE NEWEST-DATE)"
  (with-current-buffer (get-file-buffer (or diet-file org-diet-file))
    (save-excursion
      (beginning-of-buffer)
      (search-forward-regexp "^\\* Daily Logs[ \t]*$")
      (show-subtree)
      (org-diet-weight-subtree-entries-at-point))))


(defun org-diet-graphable-from-date-table (date-table date-oldest date-newest)
  "Returns a list of lists in the form:
   '((date weight averaged-weight accuracy))"
  (delq
   nil
   (mapcar
    (lambda (date)
      (when (org-diet-get-date-prop date-table date "weight")
          (list
           (car (split-string date))
           (org-diet-get-date-prop date-table date "weight")
           (org-diet-get-date-prop date-table date "averaged-weight")
           (org-diet-get-date-prop date-table date "weight-accuracy"))))
    (org-diet-get-date-range-by-dates date-oldest date-newest))))


;; Convencience funcs for getting graphable data

(defun org-diet-graphable-all-time ()
  (org-diet-graphable-from-date-table
   org-diet-date-table
   org-diet-oldest-date org-diet-newest-date))


(defun org-diet-graphable-year-start-date ()
  (format-time-string
   "%Y-%m-%d"
   (org-time-from-absolute
    (- (time-to-days (current-time)) 365))))


(defun org-diet-graphable-month-start-date ()
  (format-time-string
   "%Y-%m-%d"
   (org-time-from-absolute
    (- (time-to-days (current-time)) 30))))


(defun org-diet-graphable-today-date ()
  (format-time-string
   "%Y-%m-%d"
   (current-time)))


(defun org-diet-graphable-last-month ()
  (org-diet-graphable-from-date-table
   org-diet-date-table
   (format-time-string
    "%Y-%m-%d %a"
    (org-time-from-absolute
     ;; We grab 2 months worth just to make sure we have enough data
     ;; in case we slacked the previous month ;p
     (- (org-time-string-to-absolute org-diet-newest-date) 60)))
   org-diet-newest-date))


(defun org-diet-graphable-last-year ()
  (org-diet-graphable-from-date-table
   org-diet-date-table
   (format-time-string
    "%Y-%m-%d %a"
    (org-time-from-absolute
     ;; We grab an extra month's worth just to make sure we have enough data
     ;; in case we slacked the previous month ;p
     (- (org-time-string-to-absolute org-diet-newest-date) 395)))
   org-diet-newest-date))


(defun org-diet-set-date-properties (date-table date properties)
  "Set PROPERTIES for DATE in DATE-TABLE

PROPERTIES is an alist of (prop . value)
DATE-TABLE is a table of tables, where the sub-tables are
)properites for various dates."
  (let ((date-props (gethash date date-table)))
    ;; Set the sub-table if one doesn't exist
    (if (not date-props)
        (progn (setq date-props (make-hash-table :test 'equal))
               (puthash date date-props date-table)))
    (dolist (prop-cons properties)
      (puthash (car prop-cons) (cdr prop-cons) date-props))))


;; Fast!
(defun org-diet-date-list-to-table (date-list)
  "Convert a date-list to a date-table"
  (let ((date-table (make-hash-table :test 'equal)))
    (dolist (date-cons date-list date-table)
      (org-diet-set-date-properties date-table (car date-cons) (cdr date-cons)))))


(defun org-diet-get-date-prop (date-table date prop)
  (let ((date-props (gethash date date-table)))
    (if date-props
        (gethash prop date-props))))

(defun org-diet-data-as-json (diet-data)
  "Take DIET-DATA, return it as a json string"
  (json-encode
   (mapcar (lambda (entry)
             (cons (cons 'date (substring-no-properties (car entry)))
                   (mapcar (lambda (pair)
                             (cons (make-symbol (car pair))
                                   (if (stringp (cdr pair))
                                       (substring-no-properties (cdr pair))
                                     (cdr pair))))
                           (cdr entry))))
           (car diet-data))))

(defun write-diet-data-to-file (filename &optional diet-data)
  (let ((file (find-file-noselect filename)))
    (save-excursion
      (switch-to-buffer file)
      (delete-region (point-min) (point-max))
      (insert (org-diet-data-as-json (or diet-data
                                         (org-diet-gather-date-list))))
      (json-pretty-print-buffer)
      (save-buffer nil)
      (kill-buffer))))

(defun org-diet-append-weight-average (date-table date &optional average-length)
  "Add average weight data to current date"
  (let* ((average-length (or average-length 10))
         (date-range
          (org-diet-get-date-range date average-length))
         (weights (delq nil
                        (mapcar
                         (lambda (this-date)
                           (org-diet-get-date-prop date-table this-date "weight"))
                         date-range)))
         (date-length (safe-length weights))
         (averaged-weight (/ (apply '+ weights) date-length))
         (weight-accuracy (/ (float date-length) average-length)))
    (org-diet-set-date-properties
     date-table date
     `(("averaged-weight" . ,averaged-weight)
       ("weight-accuracy" . ,weight-accuracy)))))


(defvar org-diet-post-gathering-analysis-funcs
  '(org-diet-append-weight-average)
  "A list of functions that append data after everything has been gathered")

(defun org-diet-append-post-gathering-analysis (date-table)
  (maphash
   (lambda (date date-props)
     (mapc
      (lambda (analysis-func)
        (apply analysis-func (list date-table date)))
      org-diet-post-gathering-analysis-funcs))
   date-table))


(defun org-diet-gather-and-process ()
  "This is the general function for gathering and processing date information.

Returns a list of (DATES OLDEST-DATE NEWEST-DATE)"
  (let* ((date-results (org-diet-gather-date-list))
         (date-table (org-diet-date-list-to-table
                      (car date-results)))
         (oldest-date (nth 1 date-results))
         (newest-date (nth 2 date-results)))
    (org-diet-append-post-gathering-analysis date-table)
    ;; TODO: Add hooks
    (list date-table oldest-date newest-date)))


(defun org-diet-fetch-high-low (date-table dates &optional include-averaged)
  "Fetch the high and the low numbers from a series of dates"
  (cl-flet ((weight-filter (prop)
                           (delq nil
                                 (mapcar
                                  (lambda (date)
                                    (org-diet-get-date-prop date-table date prop))
                                  dates))))
    (let ((weights (weight-filter "weight")))
      (if include-averaged
          (append weights
                  (weight-filter "averaged-weight")))
      (list
       (apply 'min weights)
       (apply 'max weights)))))


(defun org-diet-percentage-property-appears (date-table property num-days
                                                        &optional start-date)
  (let* ((start-date (or start-date (org-diet-time-string-today)))
         (days (org-diet-get-date-range
                start-date num-days)))
    (/ (length
        (delq nil
              (mapcar
               (lambda (day)
                 (org-diet-get-date-prop
                  date-table day property))
               days)))
       (float num-days))))


(defun org-diet-percentage-property-matches (date-table property matches num-days
                                                        &optional start-date)
  (let* ((start-date (or start-date (org-diet-time-string-today)))
         (days (org-diet-get-date-range
                start-date num-days)))
    (/ (length
        (delq nil
              (mapcar
               (lambda (day)
                 (equal
                  (org-diet-get-date-prop
                   date-table day property)
                  matches))
               days)))
       (float num-days))))


(defun org-diet-get-soonest-property (date-table property difference
                                                 &optional
                                                 start-date oldest-date)
  "Get the soonest property with the DIFFERENCE of days from START-DATE we can

Make sure that the date we produce doesn't get older than the oldest-date

Returns a list of (PROPERTY OFFSET) where OFFSET is the number of
days we had to go back to get the property.

If no property can be found, return nil"
  (if (not (or oldest-date org-diet-oldest-date))
      (error
       "one of oldest-date or org-diet-oldest-date global variable must be set"))
  (let* ((start-date (or start-date (org-diet-time-string-today)))
         (offset 0)
         (result nil)
         (maximum-offset
          (org-diet-time-string-difference
           start-date (or oldest-date org-diet-oldest-date))))
    (while (or (not result)
               (> offset maximum-offset))
      (setq result
            (org-diet-get-date-prop
             date-table
             (org-diet-time-string-from-difference
              start-date
              (* -1 (+ difference offset)))
             property))
      (when (not result)
          (setq offset (+ offset 1))))
    (if result
        (list result offset))))


(defvar org-diet-expansion-template
  (when load-file-name
      (concat (file-name-directory load-file-name) "results_template.org"))
  "Path to an org-diet expansion template for showing results.
Uses yasnippets and orgmode, yo.")


(defvar org-diet-save-progress-to-file
  "/tmp/org_diet_results.org")


(defvar org-diet-date-table nil
  "Cached version of the date table")
(defvar org-diet-oldest-date nil
  "Cached version of the oldest date")
(defvar org-diet-newest-date nil
  "Cached version of the newest date")

(defvar org-diet-expand-env-func
  'org-diet-default-template-expand
  "Function (if any) to provide variables for the esxpansion environment")

(defun org-diet-expand-progress-template ()
  "Expand the org-diet expansion template to show off progress results

Note: this DELETES any content might currently be in said buffer!"
  ;; Create the buffer
  (if (not org-diet-expansion-template)
      (error "No expansion template given, no way to expand."))
  (let ((date-results (org-diet-gather-and-process)))
    (setq org-diet-date-table (car date-results))
    (setq org-diet-oldest-date (nth 1 date-results))
    (setq org-diet-newest-date (nth 2 date-results)))

  (if org-diet-save-progress-to-file
      (find-file-other-window org-diet-save-progress-to-file)
    (switch-to-buffer-other-window "*org-diet-report*"))
  (erase-buffer)
  (org-mode)

  ;; Expand
  (let ((yas-indent-line))
    (yas-expand-snippet
     (with-temp-buffer
       (insert-file-contents org-diet-expansion-template)
       (buffer-string))
     nil nil
     (if org-diet-expand-env-func
         (apply org-diet-expand-env-func nil))))
  (org-babel-execute-buffer)
  (beginning-of-buffer)
  (if org-diet-save-progress-to-file
      (save-buffer)))

;; --------------------------------------------
;; This is built assuming the standard template
;; --------------------------------------------

(defun org-diet-default-template-expand ()
  (let ((present-weight
         (org-diet-get-date-prop
          org-diet-date-table org-diet-newest-date "weight"))
        (previous-weigh-in-date
         (org-diet-time-string-from-difference
          org-diet-newest-date
          (- -1
             (nth 1
                  (org-diet-get-soonest-property
                   org-diet-date-table
                   "weight" 1 org-diet-newest-date))))))
    ;; Append present weight

    `((present-weight ,present-weight)
      (previous-weigh-in-date ,previous-weigh-in-date)
      (weight-for-date-with-analysis
       ,(lambda (date)
          (let* ((day-weight
                  (org-diet-get-date-prop
                   org-diet-date-table date "weight"))
                 (day-average-weight
                  (org-diet-get-date-prop
                   org-diet-date-table date "averaged-weight"))
                 (day-average-accuracy
                  (floor
                   (* 100 
                      (org-diet-get-date-prop
                       org-diet-date-table
                       date
                       "weight-accuracy"))))
                 (previous-day-weight
                  (car (org-diet-get-soonest-property
                        org-diet-date-table
                        "weight" 1 date)))
                 (previous-day-average-weight
                  (car (org-diet-get-soonest-property
                        org-diet-date-table
                        "averaged-weight" 1 date)))
                 (weight-difference
                  (- previous-day-weight day-weight))
                 (calorie-difference
                  (* weight-difference 3500))
                 (up-down
                  (if (>= weight-difference 0)
                      "down"
                    "up"))
                 (average-weight-difference
                  (- previous-day-average-weight day-average-weight))
                 (average-calorie-difference
                  (* average-weight-difference 3500))
                 (average-up-down
                  (if (>= average-weight-difference 0)
                      "down"
                    "up")))
            (concat
             (format
              " - *Weight:* %.1f  /(%s %.1f pounds)/"
              day-weight up-down
              (abs weight-difference))
             "\n"
             (format
              " - *10 day average:* %.1f  /(%s %.1f pounds / %d cals; %s%% accurate)/"
              day-average-weight average-up-down
              (abs average-weight-difference) (abs average-calorie-difference)
              day-average-accuracy)))))
      (current-bmi
       ,(let ((current-weight
               (org-diet-get-date-prop
                org-diet-date-table org-diet-newest-date "weight"))
              (height 73))
          (format "%.1f" (/ (* current-weight 703) (* height height)))))
      (weighed-in-percent-for-days
       ,(lambda (days)
          (format "%s%% of the time"
                  (floor
                   (* 100
                      (org-diet-percentage-property-appears
                       org-diet-date-table "weight" days))))))
      (clocked-out-percent-for-days
       ,(lambda (days)
          (format "%s%% of the time"
                  (floor
                   (* 100
                      (org-diet-percentage-property-matches
                       org-diet-date-table "todo" "CAL-OUT" days
                       (org-diet-time-string-from-difference
                        (org-diet-time-string-today)
                        -1)))))))
      (pound-calorie-analysis-for-days
       ,(lambda (days)
          (let* ((soonest-pound-data
                  (org-diet-get-soonest-property
                   org-diet-date-table
                   "averaged-weight" days org-diet-newest-date))
                 (days-since (+ (nth 1 soonest-pound-data) days))
                 (pound-difference
                  (- (car soonest-pound-data)
                     (org-diet-get-date-prop
                      org-diet-date-table
                      org-diet-newest-date
                      "averaged-weight")))
                 (lost-gained
                  (if (>= pound-difference 0)
                      "lost"
                    "gained"))
                 (cals (* 3500 (abs pound-difference))))
            (format
             " - %s pounds: %.1f
 - %s calories per day: %d %s"
             lost-gained
             (abs pound-difference)
             lost-gained
             (/ cals days-since)
             (if (> days-since days)
                 (format " (%s days)" days-since)
               ""))))))))


;; --------------------------------------------

(defvar org-diet-open-in-browser-after-export t)

(defun org-diet-expand-and-export-progress-template ()
  (interactive)
  (org-diet-expand-progress-template)
  (let ((exported-file (org-html-export-to-html nil nil nil nil)))
    (if org-diet-open-in-browser-after-export
        (org-open-file exported-file))))

(defalias 'org-diet-show-analysis 'org-diet-expand-and-export-progress-template)


(provide 'org-diet)
